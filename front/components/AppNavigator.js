import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './Home';
// import Friends from './Friends';
import Todos from './Todos';


const RootStack = createStackNavigator({
  Home: { screen: Home },
  Todos: { screen: Todos },
}, {
  initialRouteName: 'Home',
  // mode: 'modal',
  headerMode: 'none',
  transparentCard: true,
  transitionConfig: () => ({
    containerStyle: {
      backgroundColor: 'transparent',
    },
    containerStyleLight: {
      backgroundColor: 'transparent',
    },
    containerStyleDark: {
      backgroundColor: 'transparent',
    },
  }),
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
    // needs to be any to prevent type issues
});

const AppNavigator = createAppContainer(RootStack);

// Now AppNavigator is the main component for React to render
export default AppNavigator;