import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	listContainer: {
		alignItems: 'center'
	},
	noTodoText: {
		alignSelf: 'center',
		fontSize: 25,
		color: '#fff',
		paddingTop: 40
	}
});
