import {StyleSheet, Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	// container: {
	// 	width: width - 50,
	// 	borderBottomColor: '#34B8A9',
	// 	borderBottomWidth: StyleSheet.hairlineWidth,
	// 	flexDirection: 'row',
	// 	alignItems: 'center'
    // },
    container: {
        flex: 1,
        backgroundColor: '#242627',
        alignItems: 'center',
        justifyContent: 'center',
        // color: '#fff',
    },
    label: {
        color: '#fff',
        // paddingBottom: '15px',
    },
    addtodos: {
        // flex: 1,
        // backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
    },
        btnTodo: {
        backgroundColor: '#000',
        marginBottom: '10px',
    },
        chosenTask: {
        color: '#fff',
    },
	text: {
		flex: 1,
		fontWeight: '500',
		fontSize: 18,
		marginVertical: 20
	},
	unstrikeText: {
		color: '#3fb8a9'
	},
	circle: {
		width: 10,
		height: 10,
		borderRadius: 15,
		borderWidth: 3,
		marginRight: 20,
	},
	completeCircle: {
		borderColor: '#bbb',
		backgroundColor: '#bbb'
    },
});
