import {StyleSheet, Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		width: width - 50,
		borderBottomColor: '#34B8A9',
		borderBottomWidth: StyleSheet.hairlineWidth,
		flexDirection: 'row',
		alignItems: 'center'
	},
	text: {
		flex: 1,
		fontWeight: '500',
		fontSize: 18,
		marginVertical: 20
	},
	strikeText: {
		color: '#bbb',
		textDecorationLine: 'line-through'
	},
	unstrikeText: {
		color: '#3fb8a9'
	},
	circle: {
		width: 30,
		height: 30,
		borderRadius: 15,
		borderWidth: 3,
		marginRight: 20,
	},
	completeCircle: {
		borderColor: '#bbb',
		backgroundColor: '#bbb'
	},
	incompleteCircle: {
		borderColor: '#3fb8a9'
	},
	deleteIcon: {
		width: 30,
		height: 30,
		alignSelf: 'center',
		justifyContent: 'flex-end'
	}
});
