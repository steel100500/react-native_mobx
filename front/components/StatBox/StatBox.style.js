import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginLeft: 20,
		marginRight: 20,
	},
	activeTodoView: {
		flexDirection: 'column',
		alignItems: 'center'
	},
	activeTextLabel: {
		fontSize: 22,
		color: 'green'
	},
	activeText: {
		fontSize: 23,
		fontWeight: '500',
		color: 'green'
	},
	remainingTodoView: {
		flexDirection: 'column',
		alignItems: 'center',
	},
	remainingTextLabel: {
		fontSize: 22,
		color: 'green'
	},
	remainingText: {
		fontSize: 23,
		fontWeight: '500',
		color: 'green'
	}
});
