import React from 'react';
import { StyleSheet, Text, View, Button, StatusBar } from 'react-native';
import {inject, observer} from 'mobx-react';
import CardView from './CardView/CardView';

@inject('todoStore')
@observer
export default class Home extends React.Component {
  componentWillMount = () => {
    this.props.todoStore.saveTodo();
  }
  render() {
    // console.log('HOME', this.props.todoStore)
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <CardView />
        <Text style={styles.preparedTittle}>Also we have {this.props.todoStore.preparedTodoCount} prepared todos!</Text>
        <Button
          title="Add prepared todos"
          onPress={() =>
            this.props.navigation.navigate('Todos')
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#242627',
    alignItems: 'center',
    justifyContent: 'center',
  },
  preparedTittle: {
		alignSelf: 'center',
		fontSize: 25,
		color: '#fff',
		paddingTop: 40
	}
});