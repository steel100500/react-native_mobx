import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { observer, inject } from 'mobx-react';
import styles from './Todos.style';

@inject('todoStore')
@observer
export default class Todos extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      countChanges: 0,
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Choose prepared todos here!</Text>
        {
          this.props.todoStore.preparedTodos.map((todo, index) => (
            <Button
              key={todo}
              title={ `Add ${ todo }` }
              onPress={() => {
                this.props.todoStore.pushPreparedTodo(index),
                this.setState({ countChanges: this.props.todoStore.countChanges + 1 })}
              }
              style={styles.btnTodo}
            />
          )
        )
        }
        { this.props.todoStore.currentTodos.length !== 0 &&
          <ul style={{'listStyle': 'none'}}>
            {
              this.props.todoStore.currentTodos.map((el, index) => (
                <li style={{display: 'flex', AliginItems: 'center'}} key={el}><View style={[styles.circle, styles.completeCircle]} /><Text style={styles.chosenTask}>{el}</Text></li>
              ))
            }
          </ul>
        }
        <Button
          title="Back home"
          onPress={() =>
            this.props.navigation.navigate('Home')
          }
        />
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#242627',
//     alignItems: 'center',
//     justifyContent: 'center',
//     // color: '#fff',
//   },
//   label: {
//     color: '#fff',
//     paddingBottom: '15px',
//   },
//   addtodos: {
//     // flex: 1,
//     // backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     flexDirection: 'column',
//   },
//   btnTodo: {
//     backgroundColor: '#000',
//     marginBottom: '10px',
//   },
//   chosenTask: {
//     color: '#fff',
//   },
// });