import { configure } from 'mobx';

import TodoStore from './TodoStore';

configure({
  enforceActions: 'always',
});

class RootStore {
  constructor() {
    this.todoStore = TodoStore;
  }
}

export default new RootStore();
