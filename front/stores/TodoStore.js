import {observable, action, computed} from 'mobx'
import {AsyncStorage} from 'react-native';
import api from '../api';

class TodoStore {
  /**
	 * Holds the list of Todos
	 *
	 * @type {Array}
	 */
  @observable todos = []
  @observable preparedTodos = [
    'Learn ReactNative Navigator',
    'plug in the Mobx',
    'use AsyncStorage',
    'use DjangoRestFramework',
  ]
  @observable currentTodos = []

  constructor() {
    this.retreiveTodo();
  }
  @action pushPreparedTodo = (index) => {
    const addedTodo = this.preparedTodos.splice(index, 1)
    this.currentTodos.push(addedTodo)
    const title = addedTodo[0]
    this.addTodo({title})
  }

  @computed
  get totalTodo() {
    return this.todos.length;
  }

  @computed
  get unfinishTodoCount() {
    return this.todos.filter(todo => !todo.isCompleted).length;
  }

  @computed
  get completedTodo() {
    return this.todos.length - this.activeTodoCount;
  }


  @action
  object_to_arr = (obj) => {
    var result = Object.keys(obj).map(function(key) {
      return Number(key), obj[key];
    });
    return result
  }

  @action
  retreiveTodo = async () => {
    try {
      this.todos = [];
      // const getTodos = await AsyncStorage.getItem('todos');
      const getTodos = await api.get(`http://192.168.1.72:8000/api/todos/`);
      // const parsedTodo = JSON.parse(result) || [];
      const result = this.object_to_arr(getTodos.results)
      const parsedTodo = result || [];
      parsedTodo.forEach((el)=>{
        const id = el['id']
        const title = el['title']
        const completed = el['completed']
        // this.addTodo({id, title, completed})
        // console.log('==============',this.todos)
        this.saveTodos(id, title, completed);
        // this.saveTodo();
      })
      // const todos = parsedTodo.map((el) => {
      //   const id = el['id']
      //   const title = el['title']
      //   const completed = el['completed']
      //   // this.addTodo({id, title, completed})
      //   // this.saveTodo();
      //   const item = new TodoModel(id, title, completed);
      //   return item;
      // });
      // console.log('todos', todos);
      // this.todos = todos;

      // this.todos = [...parsedTodo];
    } catch (error) {
      console.log('retreiveTodo:', error)
    }
  }
  @action
  saveTodos = (id, title, completed) => {
    this.todos.unshift(new TodoModel(id, title, completed));
    this.saveTodo();
  }

  @action
  saveTodo = async () => {
    try {
      await AsyncStorage.setItem('todos', JSON.stringify(this.todos))
    } catch (error) {
      console.log('saveTodo:', error)
    }
  }

  @action
  addTodo = ({id, title, completed} = {}) => {
    // this.todos.unshift(new TodoModel(id, title, completed));
    this.post(title)
    // this.saveTodo();
  }

  @action
  post = async (title) => {
    try {
      const payload = {title};
      const postTodo = await api.post(`http://192.168.1.72:8000/api/todos/`, payload);
      if (postTodo) {
        const {id, title, completed} = postTodo
        this.saveTodos(id, title, completed);
      }
    } catch (error) {
      console.log('addTodo Error:', error)
    }
  }

  @action
  markTodo = async (id) => {
    const todo = this.todos.find(todo => todo.id === id);
    todo.isCompleted = !todo.isCompleted;
    const completed = todo.isCompleted
    const payload = {completed};
    if (todo.isCompleted) {
      this.todos.push(this.todos.splice(this.todos.indexOf(todo), 1)[0]);
      await api.patch(`http://192.168.1.72:8000/api/todos/${todo.id}/`, payload);
		} else {
      this.todos.unshift(this.todos.splice(this.todos.indexOf(todo), 1)[0]);
      await api.patch(`http://192.168.1.72:8000/api/todos/${todo.id}/`, payload);
		}
		this.saveTodo();
  }

  @action
  deleteTodo = async(id) => {
		const todo = this.todos.find(todo => todo.id === id);
		const deleted = this.todos.splice(this.todos.indexOf(todo), 1);
    this.saveTodo();
    await api.delete(`http://192.168.1.72:8000/api/todos/${todo.id}`);
		return deleted;
  };

  @computed
  get unfinishedTodoCount() {
    return this.todos.length - this.todos.filter(todo => todo.isCompleted).length;
  }
  @computed
  get preparedTodoCount() {
    return this.preparedTodos.length;
  }
}

class TodoModel {
	@observable id;
	@observable title;
	@observable isCompleted;
  constructor(id, title, isCompleted) {
		this.id = id || Math.floor(Math.random() * 1000);
    this.title = title;
    this.isCompleted = isCompleted || false;
	}
}

const todoStore = new TodoStore()
export default todoStore