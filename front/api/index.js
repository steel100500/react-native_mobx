import axios from 'axios';

import {
  camelToSnake,
  snakeToCamel,
} from './object';
import sleep from './sleep';
// import { checkIsLoggedIn } from './login';

// Max count of attemts if response from the server is 5xx
const MAX_ATTEMPTS = 10;
const ATTEMPTS_SLEEP_TIME = 4000;


/**
 * Preapres payload for API request
 * @param {FormData|Object} payload - payload
 * @returns {Object} handled payload
 */
function preparePayloadRequest(payload) {
  if (payload instanceof FormData) {
    return payload;
  }

  return camelToSnake(payload);
}

/**
 * Handles error and returns message or object
 * @param {Error} error - error
 * @returns {String|Object} - response
 */
function handleErrorResponse(error) {
  if (error != null && error.response) {
    if (error.response.data) {
      const { data } = error.response;

      return typeof data === 'string' ? data : snakeToCamel(data);
    }

    return error.response;
  }

  return null;
}


const methods = {
  delete(url, headers, payload) {
    return axios.delete(url, {
      headers,
      params: payload,
    });
  },
  get(url, headers, payload) {
    return axios.get(url, {
      headers,
      params: payload,
    });
  },
  patch(url, headers, payload) {
    // console.log('==========',url, headers, payload)
    return axios.patch(url, payload, { headers });
  },
  post(url, headers, payload) {
    return axios.post(url, payload, { headers });
  },
  put(url, headers, payload) {
    return axios.put(url, payload, { headers });
  },
};

const apiCall = async (method, url, payload = null, headers = {}) => {

  // console.log(method, url, payload, headers)

  const payloadInSnakeCase = preparePayloadRequest(payload);

  let response = null;

  let attempts = 0;

  while (response == null) {
    try {
      //   checkIsLoggedIn();
      // eslint-disable-next-line no-await-in-loop
      response = await methods[method](url, headers, payloadInSnakeCase);

      return typeof response === 'string' ? response.data : snakeToCamel(response.data);
    } catch (error) {
      if (error.response == null) {
        console.log(error);

        return null;
      }

      const status = String(error.response.status);

      if (attempts >= MAX_ATTEMPTS || status[0] !== '5') {
        return handleErrorResponse(error);
      }
      attempts += 1;
      // eslint-disable-next-line no-await-in-loop
      await sleep(ATTEMPTS_SLEEP_TIME);
    }
  }

  return null;
};

export default {
  delete(...args) {
    return apiCall('delete', ...args);
  },
  get(...args) {
    return apiCall('get', ...args);
  },
  patch(...args) {
    return apiCall('patch', ...args);
  },
  post(...args) {
    return apiCall('post', ...args);
  },
  put(...args) {
    return apiCall('put', ...args);
  },

};
