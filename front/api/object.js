import { camelCase, snakeCase } from 'change-case';

export const isArray = (val) => Array.isArray(val);

export const isObject = (val) => typeof val === 'object' && !(val instanceof Date);


/**
 * @function arrayToMethod
 * @description conversion keys of array nesting objects from snake/camel notation to camel/snake notation
 * @param {array} array - handling object;
 * @param {function} method  - handle nesting object function.
 * @returns {array} - handled array
 */
const arrayToMethod = (array, method) => array.reduce((acc, item) => {
  acc.push(isObject(item) ? method(item) : item);

  return acc;
}, []);


/**
 * @function    snakeToCamel
 * @description conversion object keys(and nesting) from snake notation to camel notation
 * @param {object} obj - handling object.
 * @returns {object} - object in camelCase
 */
export const snakeToCamel = obj => {
  if (obj == null) {
    return null;
  }

  if (isObject(obj)) {
    if (isArray(obj)) return arrayToMethod(obj, snakeToCamel);

    return Object.keys(obj).reduce((acc, snakeKey) => {
      const camelKey = camelCase(snakeKey);

      acc[camelKey] = isObject(obj[snakeKey]) ? snakeToCamel(obj[snakeKey]) : obj[snakeKey];

      return acc;
    }, {});
  }

  return obj;
};

/**
 * @function    camelToSnake
 * @description conversion object keys(and nesting) from camel notation to snake notation
 * @param {object} obj - handling object.
 * @returns {object} - handled object
 */
export const camelToSnake = obj => {
  if (obj == null) {
    return null;
  }

  if (isObject(obj)) {
    if (isArray(obj)) return arrayToMethod(obj, camelToSnake);

    return Object.keys(obj).reduce((acc, camelKey) => {
      const snakeKey = snakeCase(camelKey);

      acc[snakeKey] = isObject(obj[camelKey]) ? camelToSnake(obj[camelKey]) : obj[camelKey];

      return acc;
    }, {});
  }
  throw new Error('Instance is not an object: "camelToSnake".');
};

/**
 * Check is empty object
 * @param {object} obj - hadnling object
 * @returns {boolean} - is empty object
 */
export const isEmpty = obj => Object.keys(obj).every(key => (
  Array.isArray(obj[key]) ? obj[key].length === 0 : !obj[key]
));

/**
 * Safely accessing deeply nested value
 * Usage `get(user, ['user', 'posts', 0, 'comments'])`
 * @param {object} object - object
 * @param {array} proprties - array of proprties
 * @returns {any} - value
 */
export const get = (object, proprties) => proprties.reduce((prev, current) => (
  prev && prev[current] ? prev[current] : null
), object);
