
from rest_framework_jwt.settings import api_settings

from django.db import models
from django.contrib.auth.models import User

class Todo(models.Model):
    title = models.CharField(max_length=30)
    completed = models.BooleanField(default=False)
    author = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        return self.title
    class Meta:
        ordering = ['id']
