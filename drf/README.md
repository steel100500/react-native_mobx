# INSTALL DRF
```bash
source venv/bin/activate
cd drf
pip install -r requirements.txt
```
# RUN DRF
```bash
python manage.py runserver 0.0.0.0:8000
```